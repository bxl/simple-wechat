package com.adouteam.simple.wechat.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.adouteam.simple.wechat.msg.WechatMsgType;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface SimpleWeixinMsgHandle{
    WechatMsgType type() default WechatMsgType.TEXT;
}
