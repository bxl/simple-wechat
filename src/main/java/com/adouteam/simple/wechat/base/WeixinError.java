package com.adouteam.simple.wechat.base;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import org.apache.commons.lang3.StringUtils;

public class WeixinError {
    @JSONField(name = "errcode")
    private int errorCode;
    @JSONField(name = "errmsg")
    private String errorMsg;
    private String friendlyErrorMsg;

    public WeixinError() {
    }

    public WeixinError(String weixinResponseString) {
        if (StringUtils.isNotBlank(weixinResponseString)) {
            WeixinError weixinError = JSONObject.parseObject(weixinResponseString, WeixinError.class);
            if (weixinError != null) {
                this.errorCode = weixinError.getErrorCode();
                this.errorMsg = weixinError.getErrorMsg();
                this.friendlyErrorMsg = WeixinErrorType.getByCode(weixinError.getErrorCode()).message;
            }
        }
    }

    public WeixinError(int errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getFriendlyErrorMsg() {
        return friendlyErrorMsg;
    }

    public void setFriendlyErrorMsg(String friendlyErrorMsg) {
        this.friendlyErrorMsg = friendlyErrorMsg;
    }

    @Override
    public String toString() {
        return "WeixinError{" +
                "errorCode=" + errorCode +
                ", errorMsg='" + errorMsg + '\'' +
                ", friendlyErrorMsg='" + friendlyErrorMsg + '\'' +
                '}';
    }
}
