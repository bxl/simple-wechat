package com.adouteam.simple.wechat.bean;

import org.apache.commons.lang3.ArrayUtils;

public class FaButton extends Button{
    public FaButton(String name, SubButton[] subButtonArray){
        this.name = name;
        this.sub_button = ArrayUtils.clone(subButtonArray);
    }

    private String name;
    private SubButton[] sub_button;

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public Button[] getSub_button(){
        return ArrayUtils.clone(sub_button);
    }

    public void setSub_button(SubButton[] sub_button){
        this.sub_button = ArrayUtils.clone(sub_button);
    }
}
