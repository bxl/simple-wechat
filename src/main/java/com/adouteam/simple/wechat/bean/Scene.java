package com.adouteam.simple.wechat.bean;

public class Scene{
    public int scene_id;
    public String scene_str;

    public int getScene_id(){
        return scene_id;
    }

    public void setScene_id(int scene_id){
        this.scene_id = scene_id;
    }

    public String getScene_str(){
        return scene_str;
    }

    public void setScene_str(String scene_str){
        this.scene_str = scene_str;
    }
}
