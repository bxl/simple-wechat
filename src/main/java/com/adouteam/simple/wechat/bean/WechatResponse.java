package com.adouteam.simple.wechat.bean;

public class WechatResponse{
    private long errcode;
    private String errmsg;

    public long getErrcode(){
        return errcode;
    }

    public void setErrcode(long errcode){
        this.errcode = errcode;
    }

    public String getErrmsg(){
        return errmsg;
    }

    public void setErrmsg(String errmsg){
        this.errmsg = errmsg;
    }

    @Override
    public String toString(){
        return "WeixinResponse{" +
                "errcode=" + errcode +
                ", errmsg='" + errmsg + '\'' +
                '}';
    }
}
