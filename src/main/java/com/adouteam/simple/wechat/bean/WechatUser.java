package com.adouteam.simple.wechat.bean;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.sql.Timestamp;

public class WechatUser implements Serializable{
    private static final long serialVersionUID = 5727205434895864424L;
    public static final WechatUser ME = new WechatUser();
    private long id;
    private long user;
    @JSONField(name = "openid")
    private String openId;
    private String nickname;
    private String sex;
    private String country;
    private String province;
    private String city;
    @JSONField(name = "headimgurl")
    private String headImgUrl;
    @JSONField(name = "unionid")
    private String unionId;
    private String remark;
    @JSONField(name = "subscribe_scene")
    private String subscribeScene;
    @JSONField(name = "subscribe_time")
    private Long subscribeTime;
    @JSONField(name = "qr_scene_str")
    private String qrSceneStr;
    @JSONField(name = "qr_scene")
    private Long qrScene;
    @JSONField(name = "create_time")
    private Timestamp createTime;
    private Integer subscribe;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUser() {
        return user;
    }

    public void setUser(long user) {
        this.user = user;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getHeadImgUrl() {
        return headImgUrl;
    }

    public void setHeadImgUrl(String headImgUrl) {
        this.headImgUrl = headImgUrl;
    }

    public String getUnionId() {
        return unionId;
    }

    public void setUnionId(String unionId) {
        this.unionId = unionId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSubscribeScene() {
        return subscribeScene;
    }

    public void setSubscribeScene(String subscribeScene) {
        this.subscribeScene = subscribeScene;
    }

    public Long getSubscribeTime() {
        return subscribeTime;
    }

    public void setSubscribeTime(Long subscribeTime) {
        this.subscribeTime = subscribeTime;
    }

    public String getQrSceneStr() {
        return qrSceneStr;
    }

    public void setQrSceneStr(String qrSceneStr) {
        this.qrSceneStr = qrSceneStr;
    }

    public Long getQrScene() {
        return qrScene;
    }

    public void setQrScene(Long qrScene) {
        this.qrScene = qrScene;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Integer getSubscribe() {
        return subscribe;
    }

    public void setSubscribe(Integer subscribe) {
        this.subscribe = subscribe;
    }
}
