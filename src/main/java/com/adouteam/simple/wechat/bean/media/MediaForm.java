package com.adouteam.simple.wechat.bean.media;

import java.io.File;

public class MediaForm{
    private MediaType type;
    private File media;

    public MediaType getType(){
        return type;
    }

    public void setType(MediaType type){
        this.type = type;
    }

    public File getMedia(){
        return media;
    }

    public void setMedia(File media){
        this.media = media;
    }

}
