package com.adouteam.simple.wechat.bean.media;

import java.sql.Timestamp;
import java.util.Calendar;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 音频结果
 */
public class MediaResult{
    private static final int EXPIRE_LENGTH_IN_DAY = 3;
    @JSONField
    private String type;
    @JSONField
    private String media_id;
    @JSONField
    private int created_at;

    public MediaType getType(){
        return MediaType.getMediaTypeByValue(type);
    }

    public void setType(String type){
        this.type = type;
    }

    public String getMedia_id(){
        return media_id;
    }

    public void setMedia_id(String media_id){
        this.media_id = media_id;
    }

    public int getCreated_at(){
        return created_at;
    }

    public void setCreated_at(int created_at){
        this.created_at = created_at;
    }

    public Timestamp getCreate_time(){
        return new Timestamp(created_at * 1000L);
    }

    public Timestamp getExpire_time(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(created_at * 1000L);
        calendar.add(Calendar.DAY_OF_MONTH, EXPIRE_LENGTH_IN_DAY);
        return new Timestamp(calendar.getTimeInMillis());
    }

    @Override
    public String toString(){
        return "MediaResult{" +
                "type='" + type + '\'' +
                ", media_id='" + media_id + '\'' +
                ", created_at=" + created_at + '\'' +
                ", expire_time=" + getExpire_time() +
                '}';
    }
}
