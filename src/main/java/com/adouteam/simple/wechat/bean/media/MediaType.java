package com.adouteam.simple.wechat.bean.media;

import org.apache.commons.lang3.StringUtils;

public enum MediaType {
    /**
     * 图片
     */
    IMAGE("image"),
    /**
     * 声音
     */
    VOICE("voice"),
    /**
     * 视频
     */
    VIDEO("video"),
    /**
     * 缩略图
     */
    THUMB("thumb"),
    /**
     * 未知
     */
    NULL("null");
    private String value;

    MediaType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static MediaType getMediaTypeByValue(String value) {
        for (MediaType mediaType : MediaType.values()) {
            if (StringUtils.equalsIgnoreCase(mediaType.getValue(), value)) {
                return mediaType;
            }
        }
        return NULL;
    }

}
