package com.adouteam.simple.wechat.bean.template;

import com.alibaba.fastjson.annotation.JSONField;

public class TemplateResult {
    @JSONField(name = "errcode")
    private Long errorCode;
    @JSONField(name = "errmsg")
    private String errorMsg;
    @JSONField(name = "msgid")
    private Long msgId;

    public Long getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Long errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Long getMsgId() {
        return msgId;
    }

    public void setMsgId(Long msgId) {
        this.msgId = msgId;
    }

    @Override
    public String toString() {
        return "TemplateResult{" +
                "errorCode=" + errorCode +
                ", errorMsg='" + errorMsg + '\'' +
                ", msgId=" + msgId +
                '}';
    }
}
