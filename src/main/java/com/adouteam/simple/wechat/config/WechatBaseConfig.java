package com.adouteam.simple.wechat.config;

import java.util.List;

import com.adouteam.simple.wechat.base.WechatConstants;

/**
 * 微信基本配置
 *
 * @author buxianglong
 */
public interface WechatBaseConfig{
    /**
     * 获取所有服务的微信号（OriginId）集合
     *
     * @return OriginId集合
     */
    List<String> getAppOriginIds();

    /**
     * 获取指定AppId的具体配置信息
     *
     * @param appOriginId 微信OriginId
     * @return 对应微信配置参数
     */
    WechatConstants getByAppOriginId(String appOriginId);
}
