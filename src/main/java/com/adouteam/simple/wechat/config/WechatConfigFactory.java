package com.adouteam.simple.wechat.config;

import com.adouteam.simple.wechat.tool.PropertiesTool;

/**
 * 微信配置工厂类
 *
 * @author buxianglong
 **/
public class WechatConfigFactory{
    private static final String DEFAULT_CONFIG_CLASS_NAME = "com.adouteam.simple.wechat.config.WechatConfigFromPropertiesFile";
    private static final String CONFIG_CLASS_NAME = PropertiesTool.getValue("CONFIG_CLASS_NAME", DEFAULT_CONFIG_CLASS_NAME);

//    public static WechatConfig buildWechatConfig(){
//        try{
//            return (WechatConfig) Class.forName(CONFIG_CLASS_NAME).newInstance();
//        }catch(ClassNotFoundException e){
//            e.printStackTrace();
//        }catch(IllegalAccessException e){
//            e.printStackTrace();
//        }catch(InstantiationException e){
//            e.printStackTrace();
//        }
//        return WechatConfigFromPropertiesFile.INSTANCE;
//    }
}
