package com.adouteam.simple.wechat.exception;

import com.adouteam.simple.wechat.base.WeixinError;

public class WeixinException extends Exception {
    private static final long serialVersionUID = 7283243837924916768L;
    private WeixinError weixinError;

    public WeixinException(String weixinResponseString) {
        super(new WeixinError(weixinResponseString).toString());
        this.weixinError = new WeixinError(weixinResponseString);
    }

    public WeixinException(WeixinError weixinError) {
        super(weixinError.toString());
        this.weixinError = weixinError;
    }
    
    public WeixinError getWeixinError() {
        return weixinError;
    }
}
