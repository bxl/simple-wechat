package com.adouteam.simple.wechat.msg;

/**
 * 微信消息类型枚举类
 */
public enum WechatMsgType{
    TEXT,
    IMAGE,
    VOICE,
    VIDEO,
    SHORT_VIDEO,
    LOCATION,
    LINK,
    EVENT_SUBSCRIBE,
    EVENT_UNSUBSCRIBE,
    EVENT_SCAN,
    EVENT_LOCATION,
    EVENT_MENU_CLICK,
    EVENT_MENU_VIEW,
    NULL;
}
