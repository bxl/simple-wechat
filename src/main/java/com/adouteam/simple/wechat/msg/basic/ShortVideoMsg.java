package com.adouteam.simple.wechat.msg.basic;

import com.adouteam.simple.wechat.msg.AllMsg;
import com.adouteam.simple.wechat.msg.BaseMsg;
import com.adouteam.simple.wechat.msg.MessageAble;
import com.adouteam.simple.wechat.service.ResponseService;

import javax.servlet.http.HttpServletResponse;

/**
 * 小视频消息
 *
 * @author buxianglong
 */
public class ShortVideoMsg extends BaseMsg implements MessageAble{
    private static final String MSG_TYPE = "shortvideo";
    private String mediaId;
    private String thumbMediaId;

    public String getMediaId(){
        return mediaId;
    }

    public void setMediaId(String mediaId){
        this.mediaId = mediaId;
    }

    public String getThumbMediaId(){
        return thumbMediaId;
    }

    public void setThumbMediaId(String thumbMediaId){
        this.thumbMediaId = thumbMediaId;
    }

    @Override
    public boolean isMe(AllMsg allMsg){
        if(null == allMsg){
            return false;
        }
        return MSG_TYPE.equalsIgnoreCase(allMsg.getMsgType());
    }

    @Override
    public void copyProperties(AllMsg allMsg){
        this.setFromUserName(allMsg.getFromUserName());
        this.setToUserName(allMsg.getToUserName());
        this.setCreateTime(allMsg.getCreateTime());
        this.setMsgType(allMsg.getMsgType());
        this.setMsgId(allMsg.getMsgId());
        this.setMediaId(allMsg.getMediaId());
        this.setThumbMediaId(allMsg.getThumbMediaId());
    }

    @Override
    public void process(HttpServletResponse response){
        ResponseService.responseBlank(response);
    }

    @Override
    public String toString(){
        return "ShortVideoMsg{" +
                "mediaId='" + mediaId + '\'' +
                ", thumbMediaId='" + thumbMediaId + '\'' +
                "} " + super.toString();
    }
}
