package com.adouteam.simple.wechat.msg.event;

import com.adouteam.simple.wechat.msg.BaseMsg;

/**
 * 事件类消息基类
 *
 * @author buxianglong
 */
public class EventMsg extends BaseMsg{
    static final String MSG_TYPE = "event";
    private String Event;

    public String getEvent(){
        return Event;
    }

    public void setEvent(String event){
        Event = event;
    }
}
