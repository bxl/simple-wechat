package com.adouteam.simple.wechat.msg.event;

import com.adouteam.simple.wechat.msg.AllMsg;
import com.adouteam.simple.wechat.msg.MessageAble;
import com.adouteam.simple.wechat.service.ResponseService;

import javax.servlet.http.HttpServletResponse;

/**
 * 点击菜单事件
 *
 * @author buxianglong
 */
public class MenuClickEventMsg extends EventMsg implements MessageAble{
    private static final String EVENT = "CLICK";

    private String eventKey;

    public String getEventKey(){
        return eventKey;
    }

    public void setEventKey(String eventKey){
        this.eventKey = eventKey;
    }

    @Override
    public boolean isMe(AllMsg allMsg){
        if(null == allMsg){
            return false;
        }
        return MSG_TYPE.equalsIgnoreCase(allMsg.getMsgType()) && EVENT.equalsIgnoreCase(allMsg.getEvent());
    }

    @Override
    public void copyProperties(AllMsg allMsg){
        this.setFromUserName(allMsg.getFromUserName());
        this.setToUserName(allMsg.getToUserName());
        this.setCreateTime(allMsg.getCreateTime());
        this.setMsgType(allMsg.getMsgType());
        this.setMsgId(allMsg.getMsgId());
        this.setEventKey(allMsg.getEventKey());
    }

    @Override
    public void process(HttpServletResponse response){
        ResponseService.responseBlank(response);
    }

    @Override
    public String toString(){
        return "MenuClickEventMsg{" +
                "eventKey='" + eventKey + '\'' +
                "} " + super.toString();
    }
}
