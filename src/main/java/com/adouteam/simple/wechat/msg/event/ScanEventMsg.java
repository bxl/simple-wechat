package com.adouteam.simple.wechat.msg.event;

import com.adouteam.simple.wechat.msg.AllMsg;
import com.adouteam.simple.wechat.msg.MessageAble;
import com.adouteam.simple.wechat.service.ResponseService;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletResponse;

/**
 * 扫描带参数二维码事件
 *
 * @author buxianglong
 */
public class ScanEventMsg extends EventMsg implements MessageAble {
    private static final String EVENT = "SCAN";
    private static final String EVENT_SPECIAL = "subscribe";
    public static final String EVENT_KEY_PREFIX = "qrscene_";

    private String eventKey;
    private String ticket;

    public String getEventKey() {
        return eventKey;
    }

    public void setEventKey(String eventKey) {
        this.eventKey = eventKey;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    @Override
    public boolean isMe(AllMsg allMsg) {
        if (null == allMsg) {
            return false;
        }
        if (!MSG_TYPE.equalsIgnoreCase(allMsg.getMsgType())) {
            return false;
        }
        if (EVENT.equalsIgnoreCase(allMsg.getEvent())) {
            return true;
        }
        if (EVENT_SPECIAL.equalsIgnoreCase(allMsg.getEvent()) && StringUtils.startsWithIgnoreCase(allMsg.getEventKey(), EVENT_KEY_PREFIX)) {
            return true;
        }
        return false;
    }

    @Override
    public void copyProperties(AllMsg allMsg) {
        this.setFromUserName(allMsg.getFromUserName());
        this.setToUserName(allMsg.getToUserName());
        this.setCreateTime(allMsg.getCreateTime());
        this.setMsgType(allMsg.getMsgType());
        this.setMsgId(allMsg.getMsgId());
        this.setEvent(allMsg.getEvent());
        this.setEventKey(allMsg.getEventKey());
        this.setTicket(allMsg.getTicket());
    }

    /**
     * 本次扫描是否第一次关注公众号
     *
     * @return true: 是第一次关注；false：不是
     */
    public boolean isFirstTimeSubscribe() {
        return EVENT_SPECIAL.equalsIgnoreCase(this.getEvent());
    }

    @Override
    public void process(HttpServletResponse response) {
        ResponseService.responseBlank(response);
    }

    @Override
    public String toString() {
        return "ScanEventMsg{" +
                "eventKey='" + eventKey + '\'' +
                ", ticket='" + ticket + '\'' +
                "} " + super.toString();
    }
}
