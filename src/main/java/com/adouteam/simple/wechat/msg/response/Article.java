package com.adouteam.simple.wechat.msg.response;

import com.adouteam.simple.wechat.tool.XStreamAnnotation;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 回复图文消息中的消息
 * @author buxianglong
 **/
@XStreamAlias("item")
public class Article{
    @XStreamAlias("Title")
    @XStreamAnnotation.XStreamCDATA
    private String title;
    @XStreamAlias("Description")
    @XStreamAnnotation.XStreamCDATA
    private String description;
    @XStreamAlias("PicUrl")
    @XStreamAnnotation.XStreamCDATA
    private String picUrl;
    @XStreamAlias("Url")
    @XStreamAnnotation.XStreamCDATA
    private String url;

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getDescription(){
        return description;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public String getPicUrl(){
        return picUrl;
    }

    public void setPicUrl(String picUrl){
        this.picUrl = picUrl;
    }

    public String getUrl(){
        return url;
    }

    public void setUrl(String url){
        this.url = url;
    }

    @Override
    public String toString(){
        return "Article{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", picUrl='" + picUrl + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
