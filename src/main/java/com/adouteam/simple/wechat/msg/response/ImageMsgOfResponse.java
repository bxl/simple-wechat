package com.adouteam.simple.wechat.msg.response;

import com.adouteam.simple.wechat.tool.XStreamAnnotation;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 回复图片消息
 *
 * @author buxianglong
 */
public class ImageMsgOfResponse extends BaseMsgOfResponse{
    public static final String MSG_TYPE = "image";

    public ImageMsgOfResponse(){
        this.setMsgType(MSG_TYPE);
    }

    @XStreamAlias("Image")
    @XStreamAnnotation.XStreamCDATA
    private Media media;

    public Media getMedia(){
        return media;
    }

    public void setMedia(Media media){
        this.media = media;
    }

    @Override
    public String toString(){
        return "ImageMsgOfResponse{" +
                "media=" + media +
                "} " + super.toString();
    }
}
