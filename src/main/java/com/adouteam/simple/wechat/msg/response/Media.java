package com.adouteam.simple.wechat.msg.response;

import com.adouteam.simple.wechat.tool.XStreamAnnotation;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 多媒体
 * @author buxianglong
 **/
public class Media{
    public Media(String mediaId){
        this.mediaId = mediaId;
    }

    @XStreamAlias("MediaId")
    @XStreamAnnotation.XStreamCDATA
    private String mediaId;

    public String getMediaId(){
        return mediaId;
    }

    public void setMediaId(String mediaId){
        this.mediaId = mediaId;
    }

    @Override
    public String toString(){
        return "Media{" +
                "mediaId='" + mediaId + '\'' +
                '}';
    }
}
