package com.adouteam.simple.wechat.msg.response;

import com.adouteam.simple.wechat.tool.XStreamAnnotation;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * @author buxianglong
 **/
public class Music{
    public Music(String thumbMediaId){
        this.thumbMediaId = thumbMediaId;
    }

    public Music(String title, String description, String musicUrl, String hQMusicUrl, String thumbMediaId){
        this.title = title;
        this.description = description;
        this.musicUrl = musicUrl;
        this.hQMusicUrl = hQMusicUrl;
        this.thumbMediaId = thumbMediaId;
    }

    @XStreamAlias("Title")
    @XStreamAnnotation.XStreamCDATA
    private String title;
    @XStreamAlias("Description")
    @XStreamAnnotation.XStreamCDATA
    private String description;
    @XStreamAlias("MusicUrl")
    @XStreamAnnotation.XStreamCDATA
    private String musicUrl;
    @XStreamAlias("HQMusicUrl")
    @XStreamAnnotation.XStreamCDATA
    private String hQMusicUrl;
    @XStreamAlias("ThumbMediaId")
    @XStreamAnnotation.XStreamCDATA
    private String thumbMediaId;

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getDescription(){
        return description;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public String getMusicUrl(){
        return musicUrl;
    }

    public void setMusicUrl(String musicUrl){
        this.musicUrl = musicUrl;
    }

    public String gethQMusicUrl(){
        return hQMusicUrl;
    }

    public void sethQMusicUrl(String hQMusicUrl){
        this.hQMusicUrl = hQMusicUrl;
    }

    public String getThumbMediaId(){
        return thumbMediaId;
    }

    public void setThumbMediaId(String thumbMediaId){
        this.thumbMediaId = thumbMediaId;
    }

    @Override
    public String toString(){
        return "Music{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", musicUrl='" + musicUrl + '\'' +
                ", hQMusicUrl='" + hQMusicUrl + '\'' +
                ", thumbMediaId='" + thumbMediaId + '\'' +
                '}';
    }
}
