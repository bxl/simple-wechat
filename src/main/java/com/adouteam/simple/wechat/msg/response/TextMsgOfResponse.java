package com.adouteam.simple.wechat.msg.response;

import com.adouteam.simple.wechat.tool.XStreamAnnotation;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 回复文本消息
 *
 * @author buxianglong
 */
public class TextMsgOfResponse extends BaseMsgOfResponse{
    public static final String MSG_TYPE = "text";

    public TextMsgOfResponse(){
        this.setMsgType(MSG_TYPE);
    }

    @XStreamAlias("Content")
    @XStreamAnnotation.XStreamCDATA
    private String content;

    public String getContent(){
        return content;
    }

    public void setContent(String content){
        this.content = content;
    }

    @Override
    public String toString(){
        return "TextMsgOfResponse{" +
                "content='" + content + '\'' +
                '}';
    }
}
