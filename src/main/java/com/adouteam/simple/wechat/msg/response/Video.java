package com.adouteam.simple.wechat.msg.response;

import com.adouteam.simple.wechat.tool.XStreamAnnotation;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * @author buxianglong
 **/
public class Video{
    public Video(String mediaId){
        this.mediaId = mediaId;
    }

    public Video(String mediaId, String title, String description){
        this.mediaId = mediaId;
        this.title = title;
        this.description = description;
    }

    @XStreamAlias("MediaId")
    @XStreamAnnotation.XStreamCDATA
    private String mediaId;
    @XStreamAlias("Title")
    @XStreamAnnotation.XStreamCDATA
    private String title;
    @XStreamAlias("Description")
    @XStreamAnnotation.XStreamCDATA
    private String description;

    public String getMediaId(){
        return mediaId;
    }

    public void setMediaId(String mediaId){
        this.mediaId = mediaId;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getDescription(){
        return description;
    }

    public void setDescription(String description){
        this.description = description;
    }

    @Override
    public String toString(){
        return "Video{" +
                "mediaId='" + mediaId + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
