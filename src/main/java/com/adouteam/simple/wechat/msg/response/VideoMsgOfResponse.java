package com.adouteam.simple.wechat.msg.response;

import com.adouteam.simple.wechat.tool.XStreamAnnotation;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 回复视频消息
 *
 * @author buxianglong
 */
public class VideoMsgOfResponse extends BaseMsgOfResponse{
    public static final String MSG_TYPE = "video";

    public VideoMsgOfResponse(){
        this.setMsgType(MSG_TYPE);
    }

    @XStreamAlias("Video")
    private Video video;

    public Video getVideo(){
        return video;
    }

    public void setVideo(Video video){
        this.video = video;
    }

    @Override
    public String toString(){
        return "VideoMsgOfResponse{" +
                "video=" + video +
                "} " + super.toString();
    }
}
