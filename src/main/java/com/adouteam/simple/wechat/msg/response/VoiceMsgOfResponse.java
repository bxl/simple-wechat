package com.adouteam.simple.wechat.msg.response;

import com.adouteam.simple.wechat.tool.XStreamAnnotation;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 回复语音消息
 *
 * @author buxianglong
 */
public class VoiceMsgOfResponse extends BaseMsgOfResponse{
    public static final String MSG_TYPE = "voice";

    public VoiceMsgOfResponse(){
        this.setMsgType(MSG_TYPE);
    }

    @XStreamAlias("Voice")
    @XStreamAnnotation.XStreamCDATA
    private Media media;

    public Media getMedia(){
        return media;
    }

    public void setMedia(Media media){
        this.media = media;
    }

    @Override
    public String toString(){
        return "VoiceMsgOfResponse{" +
                "media=" + media +
                "} " + super.toString();
    }
}
