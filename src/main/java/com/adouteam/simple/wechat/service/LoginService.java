package com.adouteam.simple.wechat.service;

import com.adouteam.simple.wechat.base.WechatConstants;
import com.adouteam.simple.wechat.base.WeixinUrls;
import com.adouteam.simple.wechat.tool.LinkTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * 开放平台-微信登录功能服务
 *
 * @author buxianglong
 */
public class LoginService {
    private static final Logger logger = LoggerFactory.getLogger(LoginService.class);

    /**
     * 构建"微信授权登录请求"url
     *
     * @param wechatConstants 微信配置参数
     * @param redirectUri     回调地址
     * @param state           状态值
     * @return 请求参数
     */
    public static String buildUrl(WechatConstants wechatConstants, String redirectUri, String state) {
        Map<String, String> params = new HashMap<String, String>(3) {{
            put("REDIRECT_URI", LinkTool.encodeUrl(redirectUri));
            put("SCOPE", "snsapi_login");
            put("STATE", state);
        }};
        return WeixinUrls.replace(WeixinUrls.WEB_LOGIN_OAUTH2_URL, wechatConstants, params);
    }
}
