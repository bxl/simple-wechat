package com.adouteam.simple.wechat.service;

import com.adouteam.simple.wechat.msg.BaseMsg;
import com.adouteam.simple.wechat.msg.response.*;
import com.adouteam.simple.wechat.tool.XmlTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 回复即时消息相关服务
 *
 * @author buxianglong
 */
public class ResponseService{
    private static final Logger logger = LoggerFactory.getLogger(ResponseService.class);
    private static final String RESPONSE_SUCCESS = "success";
    private static final String RESPONSE_BLANK = "";
    private static final String ENCODING = "utf-8";

    /**
     * response a "success" string, not wrapped in xml
     *
     * @param response 回复对象
     */
    public static void responseSuccess(HttpServletResponse response){
        try{
            response.setCharacterEncoding(ENCODING);
            response.getWriter().write(RESPONSE_SUCCESS);
        }catch(IOException e){
            logger.error("Failed to responseSuccess", e);
        }
    }

    /**
     * response a blank string, not wrapped in xml
     *
     * @param response 回复对象
     */
    public static void responseBlank(HttpServletResponse response){
        try{
            response.setCharacterEncoding(ENCODING);
            response.getWriter().write(RESPONSE_BLANK);
        }catch(IOException e){
            logger.error("Failed to responseSuccess", e);
        }
    }

    /**
     * response a string, not wrapped in xml
     *
     * @param response 回复对象
     * @param content  回复内容
     */
    public static void responseString(HttpServletResponse response, String content){
        try{
            response.setCharacterEncoding(ENCODING);
            response.getWriter().write(content);
        }catch(IOException e){
            logger.error("Failed to responseSuccess", e);
        }
    }

    /**
     * 回复文本消息
     *
     * @param response 请求对象
     * @param baseMsg  接受消息
     * @param content  回复文本内容
     */
    public static void responseText(HttpServletResponse response, BaseMsg baseMsg, String content){
        response.setCharacterEncoding(ENCODING);
        if(null == baseMsg){
            responseBlank(response);
        }
        TextMsgOfResponse textMsgOfResponse = new TextMsgOfResponse();
        textMsgOfResponse.setFromUserName(baseMsg.getToUserName());
        textMsgOfResponse.setToUserName(baseMsg.getFromUserName());
        textMsgOfResponse.setCreateTime(String.valueOf(System.currentTimeMillis()));
        textMsgOfResponse.setContent(content);
        String responseText = XmlTool.parseObjToXml(textMsgOfResponse);
        logger.debug("responseText:" + responseText);
        try{
            response.getWriter().print(responseText);
        }catch(IOException e){
            logger.error("Failed to responseText", e);
            e.printStackTrace();
        }
    }

    /**
     * 回复图片消息
     *
     * @param response       请求对象
     * @param baseMsg        接受消息
     * @param mediaIdOfImage 回复图片多媒体id
     */
    public static void responseImage(HttpServletResponse response, BaseMsg baseMsg, String mediaIdOfImage){
        response.setCharacterEncoding(ENCODING);
        if(null == baseMsg){
            responseBlank(response);
        }
        ImageMsgOfResponse imageMsgOfResponse = new ImageMsgOfResponse();
        imageMsgOfResponse.setFromUserName(baseMsg.getToUserName());
        imageMsgOfResponse.setToUserName(baseMsg.getFromUserName());
        imageMsgOfResponse.setCreateTime(String.valueOf(System.currentTimeMillis()));
        imageMsgOfResponse.setMedia(new Media(mediaIdOfImage));
        String responseText = XmlTool.parseObjToXml(imageMsgOfResponse);
        logger.debug("responseImage:" + responseText);
        try{
            response.getWriter().print(responseText);
        }catch(IOException e){
            logger.error("Failed to responseImage", e);
            e.printStackTrace();
        }
    }

    /**
     * 回复语音消息
     *
     * @param response       请求对象
     * @param baseMsg        接受消息
     * @param mediaIdOfVoice 回复语音媒体id
     */
    public static void responseVoice(HttpServletResponse response, BaseMsg baseMsg, String mediaIdOfVoice){
        response.setCharacterEncoding(ENCODING);
        if(null == baseMsg){
            responseBlank(response);
        }
        VoiceMsgOfResponse voiceMsgOfResponse = new VoiceMsgOfResponse();
        voiceMsgOfResponse.setFromUserName(baseMsg.getToUserName());
        voiceMsgOfResponse.setToUserName(baseMsg.getFromUserName());
        voiceMsgOfResponse.setCreateTime(String.valueOf(System.currentTimeMillis()));
        voiceMsgOfResponse.setMedia(new Media(mediaIdOfVoice));
        String responseText = XmlTool.parseObjToXml(voiceMsgOfResponse);
        logger.debug("responseVoice:" + responseText);
        try{
            response.getWriter().print(responseText);
        }catch(IOException e){
            logger.error("Failed to responseVoice", e);
            e.printStackTrace();
        }
    }

    /**
     * 返回视频消息
     *
     * @param response         请求对象
     * @param baseMsg          接受消息
     * @param mediaIdOfVideo   视频多媒体id
     * @param videoTitle       视频标题
     * @param videoDescription 视频描述
     */
    public static void responseVideo(HttpServletResponse response, BaseMsg baseMsg, String mediaIdOfVideo, String videoTitle,
                                     String videoDescription){
        response.setCharacterEncoding(ENCODING);
        if(null == baseMsg){
            responseBlank(response);
        }
        VideoMsgOfResponse videoMsgOfResponse = new VideoMsgOfResponse();
        videoMsgOfResponse.setFromUserName(baseMsg.getToUserName());
        videoMsgOfResponse.setToUserName(baseMsg.getFromUserName());
        videoMsgOfResponse.setCreateTime(String.valueOf(System.currentTimeMillis()));
        videoMsgOfResponse.setVideo(new Video(mediaIdOfVideo, videoTitle, videoDescription));
        String responseText = XmlTool.parseObjToXml(videoMsgOfResponse);
        logger.debug("responseVideo:" + responseText);
        try{
            response.getWriter().print(responseText);
        }catch(IOException e){
            logger.error("Failed to responseVideo", e);
            e.printStackTrace();
        }
    }

    /**
     * 回复音乐消息
     *
     * @param response         请求对象
     * @param baseMsg          接受消息
     * @param thumbMediaId     缩略图多媒体id
     * @param musicTitle       音乐标题
     * @param musicDescription 音乐描述
     * @param musicUrl         音乐url
     * @param hQMusicUrl       高质量音乐url
     */
    public static void responseMusic(HttpServletResponse response, BaseMsg baseMsg, String thumbMediaId, String musicTitle,
                                     String musicDescription, String musicUrl, String hQMusicUrl){
        response.setCharacterEncoding(ENCODING);
        if(null == baseMsg){
            responseBlank(response);
        }
        MusicMsgOfResponse musicMsgOfResponse = new MusicMsgOfResponse();
        musicMsgOfResponse.setFromUserName(baseMsg.getToUserName());
        musicMsgOfResponse.setToUserName(baseMsg.getFromUserName());
        musicMsgOfResponse.setCreateTime(String.valueOf(System.currentTimeMillis()));
        musicMsgOfResponse.setMusic(new Music(musicTitle, musicDescription, musicUrl, hQMusicUrl, thumbMediaId));
        String responseText = XmlTool.parseObjToXml(musicMsgOfResponse);
        logger.debug("responseMusic:" + responseText);
        try{
            response.getWriter().print(responseText);
        }catch(IOException e){
            logger.error("Failed to responseMusic", e);
            e.printStackTrace();
        }
    }

    /**
     * 回复图文消息
     *
     * @param response     请求对象
     * @param baseMsg      接受消息
     * @param articleCount 组合数量
     * @param articles     图文对象
     */
    public static void responseImageText(HttpServletResponse response, BaseMsg baseMsg, int articleCount, List<Article> articles){
        response.setCharacterEncoding(ENCODING);
        if(null == baseMsg){
            responseBlank(response);
        }
        ImageTextMsgOfResponse imageTextMsgOfResponse = new ImageTextMsgOfResponse();
        imageTextMsgOfResponse.setFromUserName(baseMsg.getToUserName());
        imageTextMsgOfResponse.setToUserName(baseMsg.getFromUserName());
        imageTextMsgOfResponse.setCreateTime(String.valueOf(System.currentTimeMillis()));
        imageTextMsgOfResponse.setArticleCount(articleCount);
        imageTextMsgOfResponse.setArticles(articles);
        String responseText = XmlTool.parseObjToXml(imageTextMsgOfResponse);
        logger.debug("responseImageText:" + responseText);
        try{
            response.getWriter().print(responseText);
        }catch(IOException e){
            logger.error("Failed to responseImageText", e);
            e.printStackTrace();
        }
    }
}
