package com.adouteam.simple.wechat.service;

import com.adouteam.simple.wechat.base.WechatConstants;
import com.adouteam.simple.wechat.base.WeixinUrls;
import com.adouteam.simple.wechat.bean.WechatUser;
import com.adouteam.simple.wechat.tool.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.util.EntityUtils;

import java.nio.charset.Charset;
import java.util.HashMap;

/**
 * 用户信息相关服务
 */
public class UserInfoService {
    /**
     * 获取微信用户信息
     *
     * @param code            code
     * @param wechatConstants 微信配置参数
     * @return 微信用户信息
     */
    public static WechatUser fetchWeixinUserInfo(final String code, WechatConstants wechatConstants) {
        if (StringUtils.isBlank(code)) {
            return null;
        }
        String accessTokenUrl = WeixinUrls.replace(WeixinUrls.AUTHORIZE_ACCESSTOKEN_URL, wechatConstants,
                new HashMap<String, String>(1) {
                    private static final long serialVersionUID = -6519113200430697093L;

                    {
                        put("CODE", code);
                    }
                });
        return HttpUtil.get(accessTokenUrl, null, (httpResponse -> {
            String responseContent = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
            JSONObject jsonObj = JSONObject.parseObject(responseContent);
            final String access_token_userinfo = jsonObj.getString("access_token");
            final String openid = jsonObj.getString("openid");
            if (StringUtils.isAnyBlank(access_token_userinfo, openid)) {
                return null;
            }
            // 拉取微信用户信息并保存
            String userInfoUrl = WeixinUrls.replace(WeixinUrls.AUTHORIZE_USERINFO_URL, wechatConstants,
                    new HashMap<String, String>(2) {
                        private static final long serialVersionUID = 2401562416601539473L;

                        {
                            put("ACCESS_TOKEN_USERINFO", access_token_userinfo);
                            put("OPENID", openid);
                        }
                    });
            return HttpUtil.get(userInfoUrl, null, (httpResponse1 -> {
                String responseContent1 = EntityUtils.toString(httpResponse1.getEntity(), "utf-8");
                WechatUser wechatUser = JSONObject.parseObject(responseContent1, WechatUser.class);
                if (wechatUser != null && StringUtils.isNotBlank(wechatUser.getOpenId())) {
                    return wechatUser;
                }
                return null;
            }));
        }));
    }

    /**
     * 获取用户appid
     *
     * @param code            code
     * @param wechatConstants 微信配置参数
     * @return 微信用户appId
     */
    public static String fetchUserAppid(final String code, WechatConstants wechatConstants) {
        if (StringUtils.isBlank(code)) {
            return null;
        }
        String accessTokenUrl = WeixinUrls.replace(WeixinUrls.AUTHORIZE_ACCESSTOKEN_URL, wechatConstants,
                new HashMap<String, String>(1) {
                    private static final long serialVersionUID = 2969118378337978844L;

                    {
                        put("CODE", code);
                    }
                });
        return HttpUtil.get(accessTokenUrl, null, (httpResponse -> {
            String responseContent = httpResponse.getEntity().getContent().toString();
            JSONObject jsonObj = JSONObject.parseObject(responseContent);
            String openid = jsonObj.getString("openid");
            if (StringUtils.isNotBlank(openid)) {
                return openid;
            } else {
                return null;
            }
        }));
    }

    /**
     * 根据OpenId获取微信用户基本信息
     *
     * @param openid          openId
     * @param wechatConstants 微信参数
     * @return 用户基本信息对象
     */
    public static WechatUser fetchWeixinUserInfoByOpenId(final String openid, WechatConstants wechatConstants) {
        if (StringUtils.isBlank(openid)) {
            return null;
        }
        String userInfoUrlOfUnion = WeixinUrls.replace(WeixinUrls.AUTHORIZE_USERINFO_UNIONID_URL, wechatConstants,
                new HashMap<String, String>(1) {
                    private static final long serialVersionUID = 5509413566316920018L;

                    {
                        put("OPENID", openid);
                    }
                });
        return HttpUtil.get(userInfoUrlOfUnion, null,
                (httpResponse -> {
                    String responseContent = EntityUtils.toString(httpResponse.getEntity(), Charset.forName("utf-8"));
                    return JSONObject.parseObject(responseContent, WechatUser.class);
                }));

    }
}
