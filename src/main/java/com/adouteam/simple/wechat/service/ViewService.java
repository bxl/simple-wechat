package com.adouteam.simple.wechat.service;

import com.adouteam.simple.wechat.base.WechatConstants;
import com.adouteam.simple.wechat.base.WeixinUrls;
import com.adouteam.simple.wechat.tool.LinkTool;

import java.util.HashMap;
import java.util.Map;

public class ViewService{
    private static String viewUrl(final String redirectUrl, final String scope, final String state, WechatConstants wechat){
        Map<String, String> params = new HashMap<String, String>(3){
            private static final long serialVersionUID = 4525746772787329865L;

            {
                put("REDIRECT_URI", LinkTool.encodeUrl(redirectUrl));
                put("SCOPE", scope);
                put("STATE", state);
            }
        };
        String url = WeixinUrls.replace(WeixinUrls.AUTHORIZE_URL, wechat, params);
        return url;
    }

    public static String viewUrl(String redirectUrl, WechatConstants wechatConstants){
        return viewUrl(redirectUrl, "", wechatConstants);
    }

    public static String viewUrl(String redirectUrl, String state, WechatConstants wechatConstants){
        return viewUrl(redirectUrl, WechatConstants.USER_INFO_SCOPE_BASE, state, wechatConstants);
    }

    public static String viewWithUserInfoUrl(String redirectUrl, WechatConstants wechatConstants){
        return viewWithUserInfoUrl(redirectUrl, "", wechatConstants);
    }

    public static String viewWithUserInfoUrl(String redirectUrl, String state, WechatConstants wechatConstants){
        return viewUrl(redirectUrl, WechatConstants.USER_INFO_SCOPE_USERINFO, state, wechatConstants);
    }
}
