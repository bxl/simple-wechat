package com.adouteam.simple.wechat.template;

public class DataOfTemplate {
    public DataOfTemplate(String value, String color) {
        this.value = value;
        this.color = color;
    }

    private String value;
    private String color;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "DataOfBase{" +
                "value='" + value + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}
