package com.adouteam.simple.wechat.template;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Map;

public class Template {
    private String template_id;
    private String touser;
    private String url;
    private String topcolor;
    @JSONField(name = "data")
    private Map<String, DataOfTemplate> dataOfTemplateMap;

    public String getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(String template_id) {
        this.template_id = template_id;
    }

    public String getTouser() {
        return touser;
    }

    public void setTouser(String touser) {
        this.touser = touser;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTopcolor() {
        return topcolor;
    }

    public void setTopcolor(String topcolor) {
        this.topcolor = topcolor;
    }

    public Map<String, DataOfTemplate> getDataOfTemplateMap() {
        return dataOfTemplateMap;
    }

    public void setDataOfTemplateMap(Map<String, DataOfTemplate> dataOfTemplateMap) {
        this.dataOfTemplateMap = dataOfTemplateMap;
    }

    public String toJson() {
        return com.alibaba.fastjson.JSON.toJSONString(this);
    }
}
