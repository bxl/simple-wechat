package com.adouteam.simple.wechat.tool;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import org.apache.commons.lang3.StringUtils;

public class LinkTool{
    public static String encodeUrl(String url){
        return encodeUrl(url, "utf-8");
    }

    public static String encodeUrl(String url, String encoding){
        try{
            if(StringUtils.isNotBlank(url)){
                return URLEncoder.encode(url, encoding);
            }
        }catch(UnsupportedEncodingException e){
        }
        return "";
    }

    public static String decodeUrl(String url){
        return decodeUrl(url, "utf-8");
    }

    public static String decodeUrl(String url, String encoding){
        try{
            if(StringUtils.isNotBlank(url)){
                return URLDecoder.decode(url, encoding);
            }
        }catch(UnsupportedEncodingException e){
        }
        return "";
    }
}
