package com.adouteam.simple.wechat.tool;

import com.thoughtworks.xstream.XStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Xml解析工具类
 *
 * @author buxianglong
 */
public class XmlTool{
    private static final Logger logger = LoggerFactory.getLogger(XmlTool.class);
    private static XStream xstream = XStreamFactory.getXStream();

    public static Object parseXmlToObj(String xml, @SuppressWarnings("rawtypes") Class type){
        xstream.alias("xml", type);
        return xstream.fromXML(xml);
    }

    public static String parseObjToXml(Object obj){
        xstream.alias("xml", obj.getClass());
        return xstream.toXML(obj);
    }
}
