package com.adouteam.simple.wechat.base;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by buxianglong on 2017/9/22.
 */
public class WeixinErrorTest {

    @Test
    public void testConstructor() {
        String errorMessage = "{\"errcode\":40125,\"errmsg\":\"invalid appsecret, view more at http:\\/\\/t.cn\\/RAEkdVq hint: [qEvQaA0872e544]\"}";
        WeixinError weixinError = new WeixinError(errorMessage);
        Assert.assertNotNull(weixinError);
        Assert.assertEquals(40125, weixinError.getErrorCode());
        Assert.assertEquals("invalid appsecret, view more at http://t.cn/RAEkdVq hint: [qEvQaA0872e544]", weixinError.getErrorMsg());
        Assert.assertEquals("未知错误", weixinError.getFriendlyErrorMsg());
    }

    @Test
    public void testConstructor2() {
        String errorMessage = "{\"errcode\":40011,\"errmsg\":\"illegal size of media file.\"}";
        WeixinError weixinError = new WeixinError(errorMessage);
        Assert.assertNotNull(weixinError);
        Assert.assertEquals(40011, weixinError.getErrorCode());
        Assert.assertEquals("illegal size of media file.", weixinError.getErrorMsg());
        Assert.assertEquals("不合法的视频文件大小", weixinError.getFriendlyErrorMsg());
    }
}
