package com.adouteam.simple.wechat.config;

import com.adouteam.simple.wechat.base.WechatConstants;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created by buxianglong on 2017/8/21.
 */
public class WechatBaseConfigFromPropertiesFileTest{
    @Test
    public void testReadMultiGroup(){
        WechatBaseConfig wechatBaseConfig = WechatBaseConfigFromPropertiesFile.INSTANCE;
        List<String> appOriginIds = wechatBaseConfig.getAppOriginIds();
        Assert.assertNotNull(appOriginIds);
        Assert.assertEquals(2, appOriginIds.size());

        WechatConstants wechatConstants = wechatBaseConfig.getByAppOriginId("gh_666666666");
        Assert.assertEquals("gh_666666666", wechatConstants.getAppOriginId());
        Assert.assertEquals("alskdhfiuashdiufia1", wechatConstants.getAppId());
        Assert.assertEquals("1234567890aaaaaaaaaaa", wechatConstants.getAppSecret());
        Assert.assertEquals("xmlx", wechatConstants.getAppToken());
        Assert.assertEquals("1q2w3e4r5t6y7u8i9o0p", wechatConstants.getAppEncodingAesKey());
        Assert.assertEquals("http://wx.adouteam2.com/", wechatConstants.getDomainName());

        WechatConstants wechatConstants1 = wechatBaseConfig.getByAppOriginId("gh_1a2b3c4d5e");
        Assert.assertEquals("gh_1a2b3c4d5e", wechatConstants1.getAppOriginId());
        Assert.assertEquals("abcdefghijklmnopqrstuvwxyz", wechatConstants1.getAppId());
        Assert.assertEquals("a1b2c3d4e5f6g7h8", wechatConstants1.getAppSecret());
        Assert.assertEquals("xllh", wechatConstants1.getAppToken());
        Assert.assertEquals("qwertyuiop", wechatConstants1.getAppEncodingAesKey());
        Assert.assertEquals("http://wx.adouteam.com/", wechatConstants1.getDomainName());
    }
}
