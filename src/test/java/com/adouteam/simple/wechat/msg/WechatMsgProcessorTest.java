package com.adouteam.simple.wechat.msg;

import com.adouteam.simple.wechat.msg.basic.*;
import com.adouteam.simple.wechat.msg.event.*;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * 微信消息处理测试
 *
 * @author buxianglong
 **/
public class WechatMsgProcessorTest{
    @Test
    public void matchType() throws Exception{
        AllMsg allMsg = new AllMsg();
        allMsg.setMsgType("text");
        assertTrue(WechatMsgProcessor.matchType(allMsg) instanceof TextMsg);
        allMsg.setMsgType("video");
        assertTrue(WechatMsgProcessor.matchType(allMsg) instanceof VideoMsg);
        allMsg.setMsgType("voice");
        assertTrue(WechatMsgProcessor.matchType(allMsg) instanceof VoiceMsg);
        allMsg.setMsgType("shortvideo");
        assertTrue(WechatMsgProcessor.matchType(allMsg) instanceof ShortVideoMsg);
        allMsg.setMsgType("location");
        assertTrue(WechatMsgProcessor.matchType(allMsg) instanceof LocationMsg);
        allMsg.setMsgType("link");
        assertTrue(WechatMsgProcessor.matchType(allMsg) instanceof LinkMsg);
        allMsg.setMsgType("image");
        assertTrue(WechatMsgProcessor.matchType(allMsg) instanceof ImageMsg);
    }

    @Test
    public void matchTypeOfEvent(){
        AllMsg allMsg = new AllMsg();
        allMsg.setMsgType("event");

        allMsg.setEvent("LOCATION");
        assertTrue(WechatMsgProcessor.matchType(allMsg) instanceof LocationEventMsg);
        allMsg.setEvent("CLICK");
        assertTrue(WechatMsgProcessor.matchType(allMsg) instanceof MenuClickEventMsg);
        allMsg.setEvent("VIEW");
        assertTrue(WechatMsgProcessor.matchType(allMsg) instanceof MenuViewEventMsg);
        allMsg.setEvent("SCAN");
        assertTrue(WechatMsgProcessor.matchType(allMsg) instanceof ScanEventMsg);
        allMsg.setEvent("subscribe");
        allMsg.setEventKey("qrscene_1234567890");
        assertTrue(WechatMsgProcessor.matchType(allMsg) instanceof ScanEventMsg);
        ScanEventMsg scanEventMsg = (ScanEventMsg) WechatMsgProcessor.matchType(allMsg);
        scanEventMsg.setEvent(allMsg.getEvent());
        assertTrue(scanEventMsg.isFirstTimeSubscribe());
        allMsg.setEvent("subscribe");
        allMsg.setEventKey(null);
        assertTrue(WechatMsgProcessor.matchType(allMsg) instanceof SubscribeEventMsg);
        allMsg.setEvent("subscribe");
        allMsg.setEventKey("123");
        assertNull(WechatMsgProcessor.matchType(allMsg));
        allMsg.setEvent("unsubscribe");
        assertTrue(WechatMsgProcessor.matchType(allMsg) instanceof UnsubscribeEventMsg);
    }
}