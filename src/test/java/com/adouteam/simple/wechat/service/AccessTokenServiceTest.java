package com.adouteam.simple.wechat.service;

import com.adouteam.simple.wechat.base.WechatConstants;
import com.adouteam.simple.wechat.testtool.TestConstants;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class AccessTokenServiceTest{
    private static AccessTokenService accessTokenService;

    @BeforeClass
    public static void init(){
        WechatConstants wechatConstants = TestConstants.initWeixinConstants();
        accessTokenService = new AccessTokenService(wechatConstants);
    }

    @Test
    public void testGetAccessTokenCache(){
        String accessToken = accessTokenService.getAccessToken();
        Assert.assertTrue(StringUtils.isNotBlank(accessToken));
    }
}
