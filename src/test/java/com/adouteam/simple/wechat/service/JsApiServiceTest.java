package com.adouteam.simple.wechat.service;

import com.adouteam.simple.wechat.base.WechatConstants;
import com.adouteam.simple.wechat.bean.JsApiSignature;
import com.adouteam.simple.wechat.testtool.TestConstants;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by buxianglong on 2017/9/21.
 */
public class JsApiServiceTest{
    private static WechatConstants wechatConstants;

    @BeforeClass
    public static void init(){
        wechatConstants = TestConstants.initWeixinConstants();
    }

    @Test
    public void testGetJsApiTicket(){
        String jsApiTicket1 = JsApiService.getJsApiTicket(wechatConstants);
        String jsApiTicket2 = JsApiService.getJsApiTicket(wechatConstants);
        Assert.assertNotNull(jsApiTicket1);
        Assert.assertNotNull(jsApiTicket2);
        Assert.assertEquals(jsApiTicket1, jsApiTicket2);
    }
    @Test
    public void testFetchJsApiSignature(){
        String url = "http://frp.adouteam.com/";
        JsApiSignature jsApiSignature = JsApiService.fetchJsApiSignature(url, wechatConstants);
        Assert.assertNotNull(jsApiSignature);
        Assert.assertTrue(StringUtils.isNotBlank(jsApiSignature.getSignature()));
    }

    @Test
    public void testCreateJsApiSignature(){
        Long timestamp = 1505980977L;
        String nonceStr = "EKQd1v46GZ3eXn6n";
        String jsApiTicket = "sM4AOVdWfPE4DxkXGEs8VFYgrfC-nsyZNem-8YZb-XHFJxwU9eXYq4DG4S1jCdaLlLw7FM4sG-dunZxCqOpVJA";
        String url = "http://ngrok.adouteam.com/wechatConstants/im";
        String jsApiSignature = JsApiService.createJsApiSignature(timestamp, nonceStr, jsApiTicket, url);
        Assert.assertEquals("573e1e817175a351f10c37de033333e2731c2124", jsApiSignature);
    }

    @Test
    public void testHelpToBuildUrl(){
        WechatConstants wechatConstants = TestConstants.initWeixinConstants();
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getRequestURI()).thenReturn("/wechatConstants/im");
        when(request.getQueryString()).thenReturn("code=23ua&state=xllh_blog_2017");
        Assert.assertEquals("http://xmlx01.adouteam.com/wechatConstants/im?code=23ua&state=xllh_blog_2017", JsApiService.helpToBuildUrl(request, wechatConstants));

        when(request.getRequestURI()).thenReturn("/wechatConstants/call_back");
        when(request.getQueryString()).thenReturn("state=xllh_blog_2017&code=23ua#linkA");
        Assert.assertEquals("http://xmlx01.adouteam.com/wechatConstants/call_back?state=xllh_blog_2017&code=23ua", JsApiService.helpToBuildUrl(request, wechatConstants));

        when(request.getRequestURI()).thenReturn("");
        when(request.getQueryString()).thenReturn("");
        Assert.assertEquals("http://xmlx01.adouteam.com", JsApiService.helpToBuildUrl(request, wechatConstants));

        when(request.getRequestURI()).thenReturn("");
        when(request.getQueryString()).thenReturn(null);
        Assert.assertEquals("http://xmlx01.adouteam.com", JsApiService.helpToBuildUrl(request, wechatConstants));

        when(request.getRequestURI()).thenReturn("/");
        when(request.getQueryString()).thenReturn(null);
        Assert.assertEquals("http://xmlx01.adouteam.com/", JsApiService.helpToBuildUrl(request, wechatConstants));

        when(request.getRequestURI()).thenReturn("/");
        when(request.getQueryString()).thenReturn("#h1");
        Assert.assertEquals("http://xmlx01.adouteam.com/", JsApiService.helpToBuildUrl(request, wechatConstants));
    }
}
