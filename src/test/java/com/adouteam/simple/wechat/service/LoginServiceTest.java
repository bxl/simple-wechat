package com.adouteam.simple.wechat.service;

import com.adouteam.simple.wechat.base.WechatConstants;
import com.adouteam.simple.wechat.testtool.TestConstants;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LoginServiceTest {
    private static WechatConstants wechatConstants;

    @BeforeClass
    public static void init() {
        wechatConstants = TestConstants.initWeixinConstants();
    }

    @Test
    public void buildUrl() {
        String expectedUrl = "https://open.weixin.qq.com/connect/qrconnect?appid=wxe022b0cd168c22f1&redirect_uri=https%3A%2F%2Fwww.xiaomalixing.net%2Fapi%2Fabc%3Fp%3D1&response_type=code&scope=snsapi_login&state=sw2020@test#wechat_redirect";
        assertEquals(expectedUrl, LoginService.buildUrl(wechatConstants, "https://www.xiaomalixing.net/api/abc?p=1", "sw2020@test"));
    }
}