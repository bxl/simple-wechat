package com.adouteam.simple.wechat.service;

import com.adouteam.simple.wechat.bean.media.MediaForm;
import com.adouteam.simple.wechat.bean.media.MediaResult;
import com.adouteam.simple.wechat.bean.media.MediaType;
import com.adouteam.simple.wechat.testtool.TestConstants;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.InputStream;

import static org.junit.Assert.*;

/**
 * @author buxianglong
 * @date 2018/09/06
 **/
public class MediaServiceTest{
    @Test
    public void uploadTempMedia() throws Exception{
        MediaForm mediaForm = new MediaForm();
        mediaForm.setType(MediaType.IMAGE);
        File file = new File(this.getClass().getResource("/oschina_1000238.jpg").getPath());
        assertTrue(file.exists());
        mediaForm.setMedia(file);
        MediaResult mediaResult = MediaService.uploadTempMedia(mediaForm, TestConstants.initWeixinConstants());
        assertNotNull(mediaResult);
        assertEquals(MediaType.IMAGE, mediaResult.getType());
        assertTrue(StringUtils.isNotBlank(mediaResult.getMedia_id()));
        System.out.println(mediaResult);
    }

    @Test
    @Ignore
    public void getTempMedia() throws Exception{
        String mediaId = "AsqMc4CCyicodJW0pnIqSOgKwCHXzifdeFErJwOdKJzkbxXomySIuIhXPHNelBGc";
        InputStream inputStream = MediaService.getTempMedia(MediaType.IMAGE, mediaId, TestConstants.initWeixinConstants());
        assertNotNull(inputStream);
        System.out.println(this.getClass().getResource("/").getPath() + "oschina_1000238_download.jpg");
        File file = new File(this.getClass().getResource("/").getPath() + "oschina_1000238_download.jpg");
        file.deleteOnExit();
        FileUtils.copyInputStreamToFile(inputStream, file);
        assertTrue(file.exists());
        assertEquals(2209, file.length());
    }

}