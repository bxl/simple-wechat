package com.adouteam.simple.wechat.service;

import com.adouteam.simple.wechat.base.WechatConstants;
import com.adouteam.simple.wechat.bean.template.TemplateResult;
import com.adouteam.simple.wechat.template.DataOfTemplate;
import com.adouteam.simple.wechat.template.Template;
import com.adouteam.simple.wechat.testtool.TestConstants;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class TemplateServiceTest {
    @Test
    public void testSendTemplate() {
        Template template = new Template();
        template.setTemplate_id("JSwaMobU7T6dHcW0zEhoV9VF5KGsdn1Qu94i-UQQctQ");
        template.setTouser("opGhyuLUCUnMxEBHzweh9aD2D7WI");
        template.setUrl(null);
        Map<String, DataOfTemplate> dataOfTemplateMap = new HashMap<>();
        dataOfTemplateMap.put("prizeLevel", new DataOfTemplate("二等奖", "#ccc"));
        dataOfTemplateMap.put("prizeDetail", new DataOfTemplate("电动自行助力车", "#ff0000"));

        template.setDataOfTemplateMap(dataOfTemplateMap);

        WechatConstants wechatConstants = TestConstants.initWeixinConstants();
        TemplateResult templateResult = TemplateService.sendTemplateToUser(template, wechatConstants);
        Assert.assertNotNull(templateResult);
        Assert.assertTrue(templateResult.getErrorCode() == 0);
        Assert.assertEquals("ok", templateResult.getErrorMsg());
    }
}
