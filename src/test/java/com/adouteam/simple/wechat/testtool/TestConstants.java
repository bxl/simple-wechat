package com.adouteam.simple.wechat.testtool;

import com.adouteam.simple.wechat.base.WechatConstants;

/**
 * Created by buxianglong on .
 */
public class TestConstants{
    public static final WechatConstants initWeixinConstants(){
        String appId = "wxe022b0cd168c22f1";
        String appSecret = "c5dbc3bb9158733b03df21148805491a";
        String appToken = "xllh";
        String appEncodingAesKey = "";
        String appOriginId = "gh_40df28b42137";
        String domainName = "http://xmlx01.adouteam.com/";
        String cacheRegionOfAccessToken = "10min";
        String cacheKeyOfAccessToken = "#accessToken#test001#";
        String cacheRegionOfJsApiTicket = "10min";
        String cacheKeyOfJsApiTicket = "#ticket#test002#";
        WechatConstants wechatConstants = new WechatConstants(appId, appSecret, appToken, appEncodingAesKey,
                appOriginId, domainName, cacheRegionOfAccessToken, cacheKeyOfAccessToken, cacheRegionOfJsApiTicket, cacheKeyOfJsApiTicket);
        return wechatConstants;
    }
}
