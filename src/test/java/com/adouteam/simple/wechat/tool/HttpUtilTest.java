package com.adouteam.simple.wechat.tool;

import org.apache.http.util.EntityUtils;
import org.junit.Test;

import java.nio.charset.Charset;

import static org.junit.Assert.assertNotNull;

/**
 * @author buxianglong
 * @date 2018/09/04
 **/
public class HttpUtilTest {
    @Test
    public void get() throws Exception {
        String content = HttpUtil.get("http://www.baidu.com/", null, (httpResponse -> {
            return EntityUtils.toString(httpResponse.getEntity(), Charset.forName("utf-8"));
        }));
        assertNotNull(content);
    }

    @Test
    public void post() throws Exception {

    }

}