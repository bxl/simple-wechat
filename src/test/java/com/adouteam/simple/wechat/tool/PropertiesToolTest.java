package com.adouteam.simple.wechat.tool;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Set;

public class PropertiesToolTest {
    @Test
    public void testGetValue() {
        String APP_ID = PropertiesTool.getValue("APP_ID");
        Assert.assertEquals("abcdefghijklmnopqrstuvwxyz", APP_ID);
        String APP_SECRET = PropertiesTool.getValue("APP_SECRET");
        Assert.assertEquals("a1b2c3d4e5f6g7h8", APP_SECRET);
        String APP_TOKEN = PropertiesTool.getValue("APP_TOKEN");
        Assert.assertEquals("xllh", APP_TOKEN);
        String APP_ENCODINGAESKEY = PropertiesTool.getValue("APP_ENCODINGAESKEY");
        Assert.assertEquals("qwertyuiop", APP_ENCODINGAESKEY);
        String APP_ORIGIN_ID = PropertiesTool.getValue("APP_ORIGIN_ID");
        Assert.assertEquals("gh_1a2b3c4d5e", APP_ORIGIN_ID);
    }

    @Test
    public void testKeySet() {
        Set<Object> keySet = PropertiesTool.keySet();
        Assert.assertEquals(18, keySet.size());
        Assert.assertTrue(keySet.containsAll(
                Arrays.asList("APP_ID", "APP_SECRET", "APP_TOKEN", "APP_ENCODINGAESKEY", "APP_ORIGIN_ID", "DOMAIN_NAME")));
    }
}
