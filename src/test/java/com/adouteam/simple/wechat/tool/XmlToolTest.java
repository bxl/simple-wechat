package com.adouteam.simple.wechat.tool;

import com.adouteam.simple.wechat.msg.AllMsg;
import com.adouteam.simple.wechat.msg.response.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Xml工具类单元测试
 * @author buxianglong
 */
public class XmlToolTest{
    @Test
    public void testTextMsgOfResponse(){
        TextMsgOfResponse textMsgOfResponse = new TextMsgOfResponse();
        textMsgOfResponse.setFromUserName("gh_40df28b42137");
        textMsgOfResponse.setToUserName("opGhyuLUCUnMxEBHzweh9aD2D7WI");
        textMsgOfResponse.setCreateTime("1543227863");
        textMsgOfResponse.setContent("你说的对啊");
        String responseText = XmlTool.parseObjToXml(textMsgOfResponse);
        assertNotNull(responseText);
        String expectedXmlStr = "<xml>\n" +
                "  <ToUserName><![CDATA[opGhyuLUCUnMxEBHzweh9aD2D7WI]]></ToUserName>\n" +
                "  <FromUserName><![CDATA[gh_40df28b42137]]></FromUserName>\n" +
                "  <CreateTime>1543227863</CreateTime>\n" +
                "  <MsgType><![CDATA[text]]></MsgType>\n" +
                "  <Content><![CDATA[你说的对啊]]></Content>\n" +
                "</xml>";
        assertEquals(expectedXmlStr, responseText);
    }

    @Test
    public void testImageMsgOfResponse(){
        ImageMsgOfResponse imageMsgOfResponse = new ImageMsgOfResponse();
        imageMsgOfResponse.setFromUserName("gh_40df28b42137");
        imageMsgOfResponse.setToUserName("opGhyuLUCUnMxEBHzweh9aD2D7WI");
        imageMsgOfResponse.setCreateTime("1543227863");
        imageMsgOfResponse.setMedia(new Media("34523452345"));
        String responseText = XmlTool.parseObjToXml(imageMsgOfResponse);
        assertNotNull(responseText);
        String expectedXmlStr = "<xml>\n" +
                "  <ToUserName><![CDATA[opGhyuLUCUnMxEBHzweh9aD2D7WI]]></ToUserName>\n" +
                "  <FromUserName><![CDATA[gh_40df28b42137]]></FromUserName>\n" +
                "  <CreateTime>1543227863</CreateTime>\n" +
                "  <MsgType><![CDATA[image]]></MsgType>\n" +
                "  <Image>\n" +
                "    <MediaId><![CDATA[34523452345]]></MediaId>\n" +
                "  </Image>\n" +
                "</xml>";
        assertEquals(expectedXmlStr, responseText);
    }

    @Test
    public void testVoiceMsgOfResponse(){
        VoiceMsgOfResponse voiceMsgOfResponse = new VoiceMsgOfResponse();
        voiceMsgOfResponse.setFromUserName("gh_40df28b42137");
        voiceMsgOfResponse.setToUserName("opGhyuLUCUnMxEBHzweh9aD2D7WI");
        voiceMsgOfResponse.setCreateTime("1543266666");
        voiceMsgOfResponse.setMedia(new Media("asdfghjkl1234567890"));
        String responseText = XmlTool.parseObjToXml(voiceMsgOfResponse);
        assertNotNull(responseText);
        String expectedXmlStr = "<xml>\n" +
                "  <ToUserName><![CDATA[opGhyuLUCUnMxEBHzweh9aD2D7WI]]></ToUserName>\n" +
                "  <FromUserName><![CDATA[gh_40df28b42137]]></FromUserName>\n" +
                "  <CreateTime>1543266666</CreateTime>\n" +
                "  <MsgType><![CDATA[voice]]></MsgType>\n" +
                "  <Voice>\n" +
                "    <MediaId><![CDATA[asdfghjkl1234567890]]></MediaId>\n" +
                "  </Voice>\n" +
                "</xml>";
        assertEquals(expectedXmlStr, responseText);
    }

    @Test
    public void testVideoMsgOfResponse(){
        VideoMsgOfResponse videoMsgOfResponse = new VideoMsgOfResponse();
        videoMsgOfResponse.setFromUserName("gh_40df28b42137");
        videoMsgOfResponse.setToUserName("opGhyuLUCUnMxEBHzweh9aD2D7WI");
        videoMsgOfResponse.setCreateTime("1543266666");
        videoMsgOfResponse.setVideo(new Video("asdfghjkl1234567890", "精彩视频123", "不错的视频"));
        String responseText = XmlTool.parseObjToXml(videoMsgOfResponse);
        assertNotNull(responseText);
        String expectedXmlStr = "<xml>\n" +
                "  <ToUserName><![CDATA[opGhyuLUCUnMxEBHzweh9aD2D7WI]]></ToUserName>\n" +
                "  <FromUserName><![CDATA[gh_40df28b42137]]></FromUserName>\n" +
                "  <CreateTime>1543266666</CreateTime>\n" +
                "  <MsgType><![CDATA[video]]></MsgType>\n" +
                "  <Video>\n" +
                "    <MediaId><![CDATA[asdfghjkl1234567890]]></MediaId>\n" +
                "    <Title><![CDATA[精彩视频123]]></Title>\n" +
                "    <Description><![CDATA[不错的视频]]></Description>\n" +
                "  </Video>\n" +
                "</xml>";
        assertEquals(expectedXmlStr, responseText);
    }

    @Test
    public void testMusicMsgOfResponse(){
        MusicMsgOfResponse musicMsgOfResponse = new MusicMsgOfResponse();
        musicMsgOfResponse.setFromUserName("gh_40df28b42137");
        musicMsgOfResponse.setToUserName("opGhyuLUCUnMxEBHzweh9aD2D7WI");
        musicMsgOfResponse.setCreateTime("1543266666");
        Music music = new Music("精彩音乐456", "不错的音乐", "MUSIC_Url", "HQ_MUSIC_Url", "media_id");
        musicMsgOfResponse.setMusic(music);
        String responseText = XmlTool.parseObjToXml(musicMsgOfResponse);
        assertNotNull(responseText);
        String expectedXmlStr = "<xml>\n" +
                "  <ToUserName><![CDATA[opGhyuLUCUnMxEBHzweh9aD2D7WI]]></ToUserName>\n" +
                "  <FromUserName><![CDATA[gh_40df28b42137]]></FromUserName>\n" +
                "  <CreateTime>1543266666</CreateTime>\n" +
                "  <MsgType><![CDATA[music]]></MsgType>\n" +
                "  <Music>\n" +
                "    <Title><![CDATA[精彩音乐456]]></Title>\n" +
                "    <Description><![CDATA[不错的音乐]]></Description>\n" +
                "    <MusicUrl><![CDATA[MUSIC_Url]]></MusicUrl>\n" +
                "    <HQMusicUrl><![CDATA[HQ_MUSIC_Url]]></HQMusicUrl>\n" +
                "    <ThumbMediaId><![CDATA[media_id]]></ThumbMediaId>\n" +
                "  </Music>\n" +
                "</xml>";
        assertEquals(expectedXmlStr, responseText);
    }

    @Test
    public void testImageTextMsgOfResponse(){
        ImageTextMsgOfResponse imageTextMsgOfResponse = new ImageTextMsgOfResponse();
        imageTextMsgOfResponse.setFromUserName("gh_40df28b42137");
        imageTextMsgOfResponse.setToUserName("opGhyuLUCUnMxEBHzweh9aD2D7WI");
        imageTextMsgOfResponse.setCreateTime("1543266666");
        int articleCount = 2;
        imageTextMsgOfResponse.setArticleCount(articleCount);
        List<Article> articleList = new ArrayList<>(articleCount);
        for(int i = 0; i < articleCount; i++){
            Article article = new Article();
            article.setTitle("图文标题" + i);
            article.setDescription("图文描述" + i);
            article.setPicUrl("图文图片链接" + i);
            article.setUrl("图文跳转链接" + i);
            articleList.add(article);
        }
        imageTextMsgOfResponse.setArticles(articleList);
        String responseText = XmlTool.parseObjToXml(imageTextMsgOfResponse);
        assertNotNull(responseText);
        String expectedXmlStr = "<xml>\n" +
                "  <ToUserName><![CDATA[opGhyuLUCUnMxEBHzweh9aD2D7WI]]></ToUserName>\n" +
                "  <FromUserName><![CDATA[gh_40df28b42137]]></FromUserName>\n" +
                "  <CreateTime>1543266666</CreateTime>\n" +
                "  <MsgType><![CDATA[news]]></MsgType>\n" +
                "  <ArticleCount>2</ArticleCount>\n" +
                "  <Articles>\n" +
                "    <item>\n" +
                "      <Title><![CDATA[图文标题0]]></Title>\n" +
                "      <Description><![CDATA[图文描述0]]></Description>\n" +
                "      <PicUrl><![CDATA[图文图片链接0]]></PicUrl>\n" +
                "      <Url><![CDATA[图文跳转链接0]]></Url>\n" +
                "    </item>\n" +
                "    <item>\n" +
                "      <Title><![CDATA[图文标题1]]></Title>\n" +
                "      <Description><![CDATA[图文描述1]]></Description>\n" +
                "      <PicUrl><![CDATA[图文图片链接1]]></PicUrl>\n" +
                "      <Url><![CDATA[图文跳转链接1]]></Url>\n" +
                "    </item>\n" +
                "  </Articles>\n" +
                "</xml>";
        assertEquals(expectedXmlStr, responseText);
    }

    @Test
    public void testParseTextMsg(){
        StringBuilder xmlStr = new StringBuilder("<xml>" +
                "<ToUserName><![CDATA[gh_40df28b42137]]></ToUserName>" +
                "<FromUserName><![CDATA[opGhyuLUCUnMxEBHzweh9aD2D7WI]]></FromUserName>" +
                "<CreateTime>1543227863</CreateTime>" +
                "<MsgType><![CDATA[mix]]></MsgType>" +
                "<MsgId>6628113202461718813</MsgId>");
        //文本消息
        xmlStr.append("<Content><![CDATA[this is a test]]></Content>");
        //图片消息
        xmlStr.append("<PicUrl><![CDATA[this is a url]]></PicUrl>");
        xmlStr.append("<MediaId><![CDATA[media_id]]></MediaId>");
        //语音消息
        xmlStr.append("<Format><![CDATA[Format]]></Format>");
        xmlStr.append("<Recognition><![CDATA[腾讯微信团队]]></Recognition>");
        //视频消息/小视频消息
        xmlStr.append("<ThumbMediaId><![CDATA[thumb_media_id]]></ThumbMediaId>");
        //地理位置消息
        xmlStr.append("<Location_X>23.134521</Location_X>");
        xmlStr.append("<Location_Y>113.358803</Location_Y>");
        xmlStr.append("<Scale>20</Scale>");
        xmlStr.append("<Label><![CDATA[位置信息]]></Label>");
        //链接消息
        xmlStr.append("<Title><![CDATA[公众平台官网链接]]></Title>");
        xmlStr.append("<Description><![CDATA[公众平台官网链接-详情]]></Description>");
        xmlStr.append("<Url><![CDATA[url]]></Url>");
        //关注/取消关注事件
        xmlStr.append("<Event><![CDATA[subscribe]]></Event>");
        //扫描带参数二维码事件
        xmlStr.append("<EventKey><![CDATA[qrscene_123123]]></EventKey>");
        xmlStr.append("<Ticket><![CDATA[TICKET]]></Ticket>");
        //上报地理位置事件
        xmlStr.append("<Latitude>23.137466</Latitude>");
        xmlStr.append("<Longitude>113.352425</Longitude>");
        xmlStr.append("<Precision>119.385040</Precision>");
        //自定义菜单事件

        xmlStr.append("</xml>");

        AllMsg allMsg = (AllMsg) XmlTool.parseXmlToObj(xmlStr.toString(), AllMsg.class);
        assertNotNull(allMsg);
        assertEquals("gh_40df28b42137", allMsg.getToUserName());
        assertEquals("opGhyuLUCUnMxEBHzweh9aD2D7WI", allMsg.getFromUserName());
        assertEquals("1543227863", allMsg.getCreateTime());
        assertEquals("mix", allMsg.getMsgType());
        assertEquals(6628113202461718813L, allMsg.getMsgId());
        //文本消息
        assertEquals("this is a test", allMsg.getContent());
        //图片消息
        assertEquals("this is a url", allMsg.getPicUrl());
        assertEquals("media_id", allMsg.getMediaId());
        //语音消息
        assertEquals("Format", allMsg.getFormat());
        assertEquals("腾讯微信团队", allMsg.getRecognition());
        //视频消息/小视频消息
        assertEquals("thumb_media_id", allMsg.getThumbMediaId());
        //地理位置消息
        assertEquals("23.134521", allMsg.getLocationX());
        assertEquals("113.358803", allMsg.getLocationY());
        assertEquals("20", allMsg.getScale());
        assertEquals("位置信息", allMsg.getLabel());
        //链接消息
        assertEquals("公众平台官网链接", allMsg.getTitle());
        assertEquals("公众平台官网链接-详情", allMsg.getDescription());
        assertEquals("url", allMsg.getUrl());
        //关注/取消关注事件
        assertEquals("subscribe", allMsg.getEvent());
        //扫描带参数二维码事件
        assertEquals("qrscene_123123", allMsg.getEventKey());
        assertEquals("TICKET", allMsg.getTicket());
        //上报地理位置事件
        assertEquals("23.137466", allMsg.getLatitude());
        assertEquals("113.352425", allMsg.getLongitude());
        assertEquals("119.385040", allMsg.getPrecision());
        //自定义菜单事件
    }
}
